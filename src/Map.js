import React from "react";
import {
  LayersControl,
  Map,
  Marker,
  Polyline,
  Popup,
  ZoomControl
} from "react-leaflet";
import { GoogleLayer } from "react-leaflet-google";

import * as utils from "./utils";
import data from "./site.json";

const { BaseLayer } = LayersControl;
const googlekey = "AIzaSyB6SCWAunJadUCr7JV-IWPAk6nSn8VleVk";

export default function MapWrapper() {
  return (
    <Map
      className="Map-container"
      center={utils.getOrigin(data)}
      bounds={utils.getMapBounds(data)}
      zoom={utils.getZoom(data)}
      zoomControl={false}
    >
      <LayersControl position="topleft">
        <BaseLayer checked name="Satellite">
          <GoogleLayer maptype="SATELLITE" googlekey={googlekey} />
        </BaseLayer>
        <BaseLayer name="Map">
          <GoogleLayer maptype="TERRAIN" googlekey={googlekey} />
        </BaseLayer>
      </LayersControl>
      <ZoomControl position="bottomright" />
      {data.Lines.map((line, index) => (
        <Polyline
          key={`line-${index}`}
          positions={line.points}
          color={line.color}
        />
      ))}
      {utils.getStations(data.Lines).map(station => (
        <Marker
          key={`station-${station.station}`}
          position={[station.lat, station.lng]}
          title={station.station}
        >
          <Popup>
            <p>{station.station}</p>
          </Popup>
        </Marker>
      ))}
    </Map>
  );
}
