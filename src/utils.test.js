import * as utils from "./utils";
import data from "./site.json";

describe("utils", () => {
  describe("getStations", () => {
    it("returns the list of station with their position", () => {
      const stations = utils.getStations(data.Lines);
      const expectedStations = [
        {
          lat: 43.53980176586632,
          lng: 1.3598995964572302,
          station: "Easymile departure"
        },
        {
          lat: 43.539857457412076,
          lng: 1.3599246507756904,
          station: "Easymile arrival"
        },
        {
          lat: 43.538816457974164,
          lng: 1.3635281676346565,
          station: "Parking"
        }
      ];

      expect(stations).toEqual(expectedStations);
    });
  });

  describe("getMapBounds", () => {
    it("returns the map bounds coordinate", () => {
      const bounds = utils.getMapBounds(data);
      const expectedBounds = [
        [43.54182451495809, 1.3593422580993124],
        [43.537571678658274, 1.3648266709129322]
      ];

      expect(bounds).toEqual(expectedBounds);
    });
  });

  describe("getZoom", () => {
    it("returns the zoom level", () => {
      const zoom = utils.getZoom(data);
      const expectedZoom = 16;

      expect(zoom).toEqual(expectedZoom);
    });
  });

  describe("getOrigin", () => {
    it("returns the origin coordinates", () => {
      const origin = utils.getOrigin(data);
      const expectedOrigin = [43.539698130087686, 1.3620845608763283];

      expect(origin).toEqual(expectedOrigin);
    });
  });
});
