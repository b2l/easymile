import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Map from "./Map";

configure({ adapter: new Adapter() });

describe("<App />", () => {
  it("renders the header and the map", () => {
    const renderedComponent = shallow(<Map />);

    expect(renderedComponent.find("Map").length).toBe(1);
    expect(renderedComponent.find("LayersControl").length).toBe(1);
    expect(renderedComponent.find("BaseLayer").length).toBe(2);
    expect(renderedComponent.find("Polyline").length).toBeGreaterThan(0);
    expect(renderedComponent.find("Marker").length).toBe(3);
  });
});
