import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Map from "./Map";
import App from "./App";

configure({ adapter: new Adapter() });

describe("<App />", () => {
  it("renders the header and the map", () => {
    const renderedComponent = shallow(<App />);

    expect(renderedComponent.find("Header").length).toBe(1);
    expect(renderedComponent.find(Map).length).toBe(1);
  });
});
