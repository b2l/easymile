export const getMapBounds = data => [
  [data.bounds.min.lat, data.bounds.min.lng],
  [data.bounds.max.lat, data.bounds.max.lng]
];

export const getOrigin = data => [data.Map.origin.lat, data.Map.origin.lng];

export const getZoom = data => data.Map.GoogleMaps.viewSettings.zoom;

// Get the list of unique stations from the list of lines
export const getStations = lines => {
  const stationsList = lines.reduce((stations, line) => {
    const station = getStation(line.points);
    if (station) {
      stations.push(station);
    }
    return stations;
  }, []);

  return uniqueStations(stationsList);
};

// Each list of points contains only one station
const getStation = points => points.find(point => !!point.station);

const uniqueStations = stations => {
  const uniqueStationsName = new Set();
  return stations.filter(station => {
    const stationName = station.station;
    return uniqueStationsName.has(stationName)
      ? false
      : uniqueStationsName.add(stationName);
  });
};
