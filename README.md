
# Easymile micro project

The goal is to reproduce this screenshot: ![./easymile-screenshot.png](./easymile-screenshot.png)

## Setup

### Quick try, docker setup
To just try it, you can use my docker image:
```
docker pull nicolasmedda/easymile
docker run --rm -p 3000:3000 nicolasmedda/easymile
```


### Local setup

clone the project and then:
```
npm i
npm start
```

This will start a local server on port 3000

If you want to run the tests:
```
npm test
```